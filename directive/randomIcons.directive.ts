﻿
/// <reference path='../_d/angularjs/angular.d.ts' />

module fusion {
    /**
     * @ngdoc directive
     * @name fnPayment
     * @module fusion
     *
     * @restrict E
     *
     * @description
     * Todo.
     */
    export function RandomIconsDirective(): ng.IDirective {
        return {
            restrict: 'E',
            scope: {},
            link: function (scope, element, attrs) {
                var icons;

                var rules;

                // reset
                icons = [];

                // find stylesheet
                angular.forEach(document.styleSheets, function (value) {
                    if (value.href && value.href.match('Style.css')) {
                        rules = value.cssRules;
                    }
                });

                // find all icon classes
                if (rules) {
                    angular.forEach(rules, function (value) {
                        var selector = value.selectorText;

                        if (selector && selector.match(/^.icon-(.+):before$/)) {
                            icons.push(selector.slice(1, selector.indexOf(':')));
                        }
                    });
                }

                var index = Math.floor(Math.random() * icons.length);

                if (window['TURNITUP']) {
                    element[0].outerHTML = '<span class=" ' + icons[index] + ' fn-spin" style="font-size: ' + Math.floor(Math.random() * (4)) + 'em;" ></span>';
                }
            }
        };
    }
}

angular.module('fusion').directive('i', fusion.RandomIconsDirective);
